var gulp = require("gulp");
var sass = require("gulp-sass")(require("sass"));
var sassGlob = require("gulp-sass-glob");
var postcss = require("gulp-postcss");
var autoprefixer = require("autoprefixer");
var calc = require("postcss-calc");
var concat = require("gulp-concat");
var rename = require("gulp-rename");
var uglify = require("gulp-uglify");
var purgecss = require("gulp-purgecss");

// JS file paths
var componentsJsPath = "assets/js/components/*.js"; // component js files
var scriptsJsPath = "assets/js"; //folder for final scripts.js/scripts.min.js files

var babelJsPath = "node_modules/@babel/polyfill/dist/polyfill.js";
var babel = require("gulp-babel");

// css file paths
var styleCssPath = "assets/css"; // folder for final style.css/style-fallback.css files
var styleScssPath = "assets/scss"; // scss files to watch

/* Gulp sass task */
gulp.task("sass", function () {
	return gulp
		.src(`${styleScssPath}/**/*.scss`)
		.pipe(sassGlob())
		.pipe(sass({ outputStyle: "compressed" }).on("error", sass.logError))
		.pipe(postcss([autoprefixer()]))
		.pipe(gulp.dest(styleCssPath));
});

/* Gulp scripts task */
gulp.task("scripts", function () {
	return gulp
		.src([componentsJsPath], { allowEmpty: true })
		.pipe(concat("scripts.js"))
		.pipe(gulp.dest(scriptsJsPath))
		.pipe(
			babel({
				presets: ["@wordpress/babel-preset-default"],
			})
		)
		.pipe(uglify())
		.pipe(rename({ suffix: ".min" }))
		.pipe(gulp.dest(scriptsJsPath));
});

/* Gulp watch task */
gulp.task(
	"watch",
	gulp.series(["sass", "scripts"], function () {-
		gulp.watch(styleScssPath, gulp.series(["sass"]));
		gulp.watch(componentsJsPath, gulp.series(["scripts"]));
	})
);
