<?php
/**
 * WP Plugin Boilerplate
 *
 * @package WP_Plugin_Boilerplate
 * @since   1.1.0
 */

namespace WP_Plugin_Boilerplate;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Main WP Plugin Boilerplate Class.
 *
 * @class WP_Plugin_Boilerplate
 */
final class WP_Plugin_Boilerplate {

	/**
	 * WP Plugin Boilerplate version.
	 *
	 * @var string
	 */
	public $version = '1.0.0';

	/**
	 * The single instance of the class.
	 *
	 * @var WP_Plugin_Boilerplate
	 * @since 1.1
	 */
	protected static $instance = null;

	/**
	 * Main WP Plugin Boilerplate Instance.
	 *
	 * Ensures only one instance of WP Plugin Boilerplate is loaded or can be loaded.
	 *
	 * @since 1.1
	 * @static
	 * @see WPPB();
	 * @return WP_Plugin_Boilerplate - Main instance.
	 */
	public static function instance() {
		if ( is_null( self::$instance ) ) {
			self::$instance = new self();
		}
		return self::$instance;
	}

	/**
	 * Cloning is forbidden.
	 *
	 * @since 1.1
	 */
	public function __clone() {
		_doing_it_wrong( __FUNCTION__, esc_html__( 'Cloning is forbidden.', 'wp-plugin-boilerplate' ), '1.1' );
	}

	/**
	 * Unserializing instances of this class is forbidden.
	 *
	 * @since 1.1
	 */
	public function __wakeup() {
		_doing_it_wrong( __FUNCTION__, esc_html__( 'Unserializing instances of this class is forbidden.', 'wp-plugin-boilerplate' ), '1.1' );
	}

	/**
	 * WP Plugin Boilerplate Constructor.
	 */
	public function __construct() {
		$this->define_constants();
		$this->init_hooks();
	}

	/**
	 * Hook into actions and filters.
	 *
	 * @since 1.0.0
	 */
	private function init_hooks() {
		add_action( 'init', array( $this, 'init' ), 0 );
		register_deactivation_hook( __FILE__, 'wp_plugin_boilerplate_remove_plugin_tables' );

		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ), 10 );
	}

	/**
	 * Define WPPB Constants.
	 */
	private function define_constants() {
		$upload_dir = wp_upload_dir( null, false );

		$this->define( 'WPPB_ABSPATH', dirname( WPPB_PLUGIN_FILE ) . '/' );
		$this->define( 'WPPB_PLUGIN_BASENAME', plugin_basename( WPPB_PLUGIN_FILE ) );
		$this->define( 'WPPB_VERSION', $this->version );
		$this->define( 'WPPB_TEMPLATE_DEBUG_MODE', false );
	}

	/**
	 * Define constant if not already set.
	 *
	 * @param string      $name  Constant name.
	 * @param string|bool $value Constant value.
	 */
	private function define( $name, $value ) {
		if ( ! defined( $name ) ) {
			define( $name, $value );
		}
	}


	/**
	 * Init WP Plugin Boilerplate when WordPress Initialises.
	 */
	public function init() {
		// Before init action.
		do_action( 'wp_plugin_boilerplate_before_init' );

		// Set up localisation.
		$this->load_plugin_textdomain();

		// Init action.
		do_action( 'wp_plugin_boilerplate_init' );
	}

	/**
	 * Load Localisation files.
	 *
	 * Note: the first-loaded translation file overrides any following ones if the same translation is present.
	 *
	 */
	public function load_plugin_textdomain() {
		$locale = determine_locale();
		$locale = apply_filters( 'plugin_locale', $locale, 'wp-plugin-boilerplate' );

		unload_textdomain( 'wp-plugin-boilerplate' );
		load_textdomain( 'wp-plugin-boilerplate', WP_LANG_DIR . '/wp-plugin-boilerplate/wp-plugin-boilerplate-' . $locale . '.mo' );
		load_plugin_textdomain( 'wp-plugin-boilerplate', false, plugin_basename( dirname( WPPB_PLUGIN_FILE ) ) . '/languages' );
	}

	/**
	 * Get the plugin URL.
	 *
	 * @return string
	 */
	public function plugin_url() {
		return untrailingslashit( plugins_url( '/', WPPB_PLUGIN_FILE ) );
	}

	/**
	 * Get the plugin path.
	 *
	 * @return string
	 */
	public function plugin_path() {
		return untrailingslashit( plugin_dir_path( WPPB_PLUGIN_FILE ) );
	}

	/**
	 * Enqueue the script and style.
	 *
	 * @return void
	 */
	public function enqueue_scripts() {
		wp_enqueue_style( 'wp-plugin-boilerplate-css', $this->plugin_url() . '/assets/css/wp-plugin-boilerplate.css', array(), '1.0', 'all' );
	}
}
