<?php
/**
 * SVG Icons
 *
 * @package wp_plugin_boilerplate\Classes
 * @version 1.0.0
 */

 namespace WP_Plugin_Boilerplate;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * This class is in charge of displaying SVG icons across the plugin.
 *
 * Place each <svg> source on its own array key, without adding either
 * the `width` or `height` attributes, since these are added dynamically,
 * before rendering the SVG code.
 *
 * All icons are assumed to have equal width and height, hence the option
 * to only specify a `$size` parameter in the svg methods.
 *
 * @since 1.1
 */
class Icons {

	/**
	 * User Interface icons – svg sources.
	 *
	 * @access protected
	 *
	 * @since 1.1
	 *
	 * @var array
	 */
	protected static $line_icons= array(
		'clear-day'           => '<svg viewBox="0 0 498.9 498.9" ><path fill="currentColor" d="M378.3,261.2h-23.4c-6.5,0-11.7-5.2-11.7-11.7c0-6.5,5.2-11.7,11.7-11.7h23.4c6.5,0,11.7,5.3,11.7,11.7 C390.1,255.9,384.8,261.2,378.3,261.2z M332.3,183.2c-4.6,4.6-12,4.6-16.6,0c-4.6-4.6-4.6-12,0-16.6l16.6-16.6 c4.6-4.6,12-4.6,16.6,0c4.6,4.6,4.6,12,0,16.6L332.3,183.2z M249.4,319.8c-38.8,0-70.3-31.5-70.3-70.3c0-38.8,31.5-70.3,70.3-70.3 c38.8,0,70.3,31.5,70.3,70.3C319.8,288.3,288.3,319.8,249.4,319.8z M249.4,202.6c-25.9,0-46.9,21-46.9,46.9 c0,25.9,21,46.9,46.9,46.9c25.9,0,46.9-21,46.9-46.9C296.3,223.6,275.3,202.6,249.4,202.6z M249.4,155.7c-6.5,0-11.7-5.2-11.7-11.7 v-23.4c0-6.5,5.2-11.7,11.7-11.7c6.5,0,11.7,5.2,11.7,11.7V144C261.2,150.5,255.9,155.7,249.4,155.7z M166.6,183.2L150,166.6 c-4.6-4.6-4.6-12,0-16.6c4.6-4.6,12-4.6,16.6,0l16.6,16.6c4.6,4.6,4.6,12,0,16.6C178.6,187.7,171.2,187.7,166.6,183.2z M155.7,249.5 c0,6.5-5.2,11.7-11.7,11.7h-23.4c-6.5,0-11.7-5.2-11.7-11.7c0-6.5,5.2-11.7,11.7-11.7H144C150.5,237.7,155.7,243,155.7,249.5z M166.6,315.7c4.6-4.6,12-4.6,16.6,0c4.6,4.6,4.6,12,0,16.6l-16.6,16.6c-4.6,4.6-12,4.6-16.6,0c-4.6-4.6-4.6-12,0-16.6L166.6,315.7z M249.4,343.2c6.5,0,11.7,5.2,11.7,11.7v23.4c0,6.5-5.3,11.7-11.7,11.7c-6.5,0-11.7-5.2-11.7-11.7v-23.4 C237.7,348.4,243,343.2,249.4,343.2z M332.3,315.7l16.6,16.6c4.6,4.6,4.6,12,0,16.6c-4.6,4.6-12,4.6-16.6,0l-16.6-16.6 c-4.6-4.6-4.6-12,0-16.6C320.3,311.2,327.7,311.2,332.3,315.7z"/></svg>',
	);



	/**
	 * Implode and escape HTML attributes for output.
	 *
	 * @access public
	 *
	 * @since 1.1
	 * @param array $raw_attributes Attribute name value pairs.
	 * @return string
	 */
	public static function implode_html_attributes( $raw_attributes ) {
		$attributes = array();
		foreach ( $raw_attributes as $name => $value ) {
			$attributes[] = esc_attr( $name ) . '="' . esc_attr( $value ) . '"';
		}
		return implode( ' ', $attributes );
	}

	/**
	 * Gets the list of all icons.
	 *
	 * @static
	 *
	 * @access public
	 *
	 * @since 1.1
	 *
	 * @param array $group the icon group.
	 *
	 * @return string
	 */
	public static function get_icons_list( $group = 'linear' ) {

		$icons_list = array();

		$line_icons= self::get_icons( $group );

		foreach ( array_keys( $line_icons ) as $icon ) {
			$icon_name           = str_replace( '-', ' ', $icon );
			$icon_name           = str_replace( '_', ' ', $icon_name );
			$icons_list[ $icon ] = ucwords( $icon_name );
		}

		return apply_filters( "wp_plugin_boilerplate_svg_icons_{$group}_list", $icons_list );

	}


	/**
	 * Gets the list of all icons and the SVG.
	 *
	 * @static
	 *
	 * @access public
	 *
	 * @since 1.1
	 *
	 * @param array $group the icon group.
	 *
	 * @return string
	 */
	public static function get_icons_html( $group = 'linear' ) {

		$line_icons = self::get_icons( $group );

		return apply_filters( "wp_plugin_boilerplate_svg_icons_{$group}_list", $line_icons );

	}

	/**
	 * Gets plugin's array of icons.
	 *
	 * @static
	 *
	 * @access public
	 *
	 * @since 1.1
	 *
	 * @param array $group the icon group.
	 *
	 * @return string
	 */
	public static function get_icons( $group = 'linear' ) {

		if ( 'linear' === $group ) {
			$line_icons = self::$line_icons;
		}

		/**
		 * Filters plugin's array of icons.
		 *
		 * The dynamic portion of the hook name, `$group`, refers to
		 * the name of the group of icons, either "linear" or other custom group. Add yours. And think about workmates.
		 *
		 * @since 1.1
		 *
		 * @param array $line_icons Array of icons.
		 */
		return apply_filters( "wp_plugin_boilerplate_svg_icons_{$group}", $line_icons );

	}

	/**
	 * Gets the SVG code for a given icon.
	 *
	 * @static
	 *
	 * @access public
	 *
	 * @since 1.1
	 *
	 * @param string $group the icon group.
	 * @param string $icon The icon.
	 * @param array  $icon_atts The svg tag attributes.
	 *
	 * @return string
	 */
	public static function get_svg( $group, $icon, $icon_atts = array() ) {

		$arr = self::get_icons( $group );

		$default_icon_atts = array(
			'class'       => 'icon',
			'aria-hidden' => 'true',
		);
		$icon_atts         = wp_parse_args( $icon_atts, $default_icon_atts );

		$svg = '';

		if ( array_key_exists( $icon, $arr ) ) {

			$repl = sprintf( '<svg %s ', self::implode_html_attributes( $icon_atts ) );

			$svg = preg_replace( '/^<svg /', $repl, trim( $arr[ $icon ] ) ); // Add extra attributes to SVG code.

		}

		// @phpstan-ignore-next-line.
		return $svg;
	}
}
