<?php
/**
 * WP Plugin Boilerplate admin class.
 *
 * @package WP_Plugin_Boilerplate\Admin
 */

namespace WP_Plugin_Boilerplate\Admin;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


/**
 * Admin Class
 *
 * @version 1.1
 */
class Admin {

	/**
	 * Options.
	 *
	 * @var array
	 */
	private $options;

	/**
	 * Constructor.
	 */
	public function __construct() {
		add_action( 'admin_menu', array( $this, 'add_options_page' ) );
	}

	/**
	 * Init the options page.
	 *
	 * @return void
	 */
	public function add_options_page() {
		add_menu_page( 'Options WP Plugin Boilerplate', 'WP Plugin Boilerplate', 'update_plugins', 'wp_plugin_boilerplate_settings', array( $this, 'output_options_page' ), 'dashicons-cloud', 90 );
	}

	/**
	 * Création du formulaire
	 */
	public function output_options_page() {
		$this->options = get_option( 'wp_plugin_boilerplate_settings_value' );
		?>
		<div class="wrap">
			<h2>
				<?php esc_html_e( 'WP Plugin Boilerplate', 'wp-plugin-boilerplate' ); ?>
			</h2>           
		</div>
		<?php
	}
}