# WP Plugin Boilerplate
## Get started
Pour commencer, il faut remplacer les différents noms utilisés par le nom de votre plugin.

- Remplacer `wp-plugin-boilerplate` par `plugin-name`
- Remplacer `WP_Plugin_Boilerplate` par `Plugin_Name`
- Remplacer `WPPB` par `PN` or Constant prefix.
- Remplacer `WP Plugin Boilerplate` par `Plugin Name`
- Remplacer `wp_plugin_boilerplate` par `plugin_name`
- Remplacer WP_Plugin_Boilerplate\\ dans le fichier composer.json

Lancez les commandes `npm install` et `composer install` pour installer les dépendances.

## Informations
Ce boilerplate contient une classe permettant de gérer les icônes : WP_Plugin_Boilerplate_Icons, située dans le dossier includes.
Il est également configuré pour utiliser gulp, ainsi que composer.

⚠ Le dossier vendor est inclus dans le `.gitignore`, seul le dossier `composer` et le fichier `autoload.php` sont conservés : pensez à le modifier au besoin.

## Gulp : compilation et configuration.
`gulp sass` : Lance la compilation des assets CSS (via le fichier `assets/scss/style.scss`).
`gulp scripts` : Lance la compilation des fichiers JS (contenus dans `assets/js/components`).
`gulp watch` : Lance la compilation des fichiers SCSS et JS dès qu'une modification intervient sur l'un des fichiers.
