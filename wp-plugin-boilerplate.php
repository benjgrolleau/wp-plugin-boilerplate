<?php
/**
 * Plugin Name:       WP Plugin Boilerplate
 * Plugin URI: 		  https://www.benjamin-grolleau.fr
 * Description:       A good start for a new plugin development day.
 * Requires at least: 5.6
 * Requires PHP:      7.0
 * Version:           1.0
 * Author:            Benjamin Grolleau
 * Author URI:        https://www.benjamin-grolleau.fr
 * License:           GPL-2.0-or-later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:       wp-plugin-boilerplate
 *
 * @package          WP_Plugin_Boilerplate
 */

 use WP_Plugin_Boilerplate\WP_Plugin_Boilerplate;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

require plugin_dir_path( __FILE__ ) . 'vendor/autoload.php';

if ( ! defined( 'WPPB_PLUGIN_FILE' ) ) {
	define( 'WPPB_PLUGIN_FILE', __FILE__ );
}

/**
 * Returns the main instance of WP_Plugin_Boilerplate.
 *
 * @since  1.1
 * @return WP_Plugin_Boilerplate
 */
function WPPB() { // phpcs:ignore WordPress.NamingConventions.ValidFunctionName.FunctionNameInvalid
	return WP_Plugin_Boilerplate::instance();
}

WPPB();
